window.onload = function() {
  setInterval(function() {
    
    // Seconds
    var seconds = new Date().getSeconds();
    document.getElementById("seconds").innerHTML = (seconds < 10 ? '0' : '') + seconds;
    if ((seconds%10) == 1){document.getElementById("seconds").style.background = "blue";}
    if ((seconds%10) == 2){document.getElementById("seconds").style.background = "red";}
    if ((seconds%10) == 3){document.getElementById("seconds").style.background = "yellow";}
    if ((seconds%10) == 4){document.getElementById("seconds").style.background = "orange";}
    if ((seconds%10) == 5){document.getElementById("seconds").style.background = "green";}

    // Minutes
    var minutes = new Date().getMinutes();
    document.getElementById("minutes").innerHTML = (minutes < 10 ? '0' : '') + minutes;
    if ((minutes%10) == 1){document.getElementById("minutes").style.background = "blue";}
    if ((minutes%10) == 2){document.getElementById("minutes").style.background = "red";}
    if ((minutes%10) == 3){document.getElementById("minutes").style.background = "yellow";}
    if ((minutes%10) == 4){document.getElementById("minutes").style.background = "orange";}
    if ((minutes%10) == 5){document.getElementById("minutes").style.background = "green";}
    // Hours
    var hours = new Date().getHours();
    document.getElementById("hours").innerHTML = (hours < 10 ? '0' : '') + hours;
    if ((hours%10) == 1){document.getElementById("hours").style.background = "blue";}
    if ((hours%10) == 2){document.getElementById("hours").style.background = "red";}
    if ((hours%10) == 3){document.getElementById("hours").style.background = "yellow";}
    if ((hours%10) == 4){document.getElementById("hours").style.background = "orange";}
    if ((hours%10) == 5){document.getElementById("hours").style.background = "green";}
  }, 0);
}
